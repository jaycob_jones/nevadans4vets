const baseurl = () => {
  return process.env.NODE_ENV === "development"
    ? "http://localhost:3000"
    : "https://golf.jaycobjones.com";
};

module.export = baseurl;
