import CssBaseline from "@material-ui/core/CssBaseline";
import { ThemeProvider } from "@material-ui/core/styles";
import { PayPalScriptProvider } from "@paypal/react-paypal-js";
import Head from "next/head";
import PropTypes from "prop-types";
import React from "react";
import theme from "../src/theme";

export default function MyApp(props) {
  const { Component, pageProps } = props;

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector("#jss-server-side");
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <React.Fragment>
      <Head>
        <title>My page</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
      </Head>
      <ThemeProvider theme={theme}>
        {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
        <CssBaseline />
        <PayPalScriptProvider
          options={{
            "client-id":
              "AZ6UuOxe4KgZiF0JEBTqt3SCdukHPw3NEfl0e0fXN2MHas9JcivNpGvJ1bRYwLGl1_gQYgJ7ahmkXy2x",
          }}
        >
          <Component {...pageProps} />
        </PayPalScriptProvider>
      </ThemeProvider>
    </React.Fragment>
  );
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
};
