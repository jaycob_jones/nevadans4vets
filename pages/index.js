import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import React from "react";
import Link from "../src/Link";

export default function Index() {
  return (
    <Container maxWidth="sm">
      <Box my={4}>
        <Link href="/registration" color="secondary">
          Go to the registration page
        </Link>
      </Box>
    </Container>
  );
}
