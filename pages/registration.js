import Box from "@material-ui/core/Box";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import baseurl from "../utils/baseurl";

const Registration = () => {
  const {
    control,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm();
  const [theGame, setTheGame] = useState(0);
  const router = useRouter();
  const onSubmit = async (data) => {
    await axios
      .post(`${baseurl()}/api/creategroup`, {
        data,
      })
      .then((res) => {
        router.push({
          pathname: "/[game]/players",
          query: { game: res.data.group_id },
        });
      });
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Container maxWidth="md">
        <Box my={4}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <InputLabel>Game</InputLabel>
              <Controller
                name="game"
                control={control}
                defaultValue="none"
                render={({ field }) => (
                  <Select fullWidth {...field}>
                    <MenuItem disabled={true} value="none">
                      <em>Select One</em>
                    </MenuItem>
                    <MenuItem value="2-person-better-ball">
                      2-Person Better Ball
                    </MenuItem>
                    <MenuItem value="4-person-scramble">
                      4-Person Scramble
                    </MenuItem>
                  </Select>
                )}
              />
            </Grid>
          </Grid>
        </Box>
        <input type="submit" value="Next Page" />
      </Container>
    </form>
  );
};

export default Registration;
