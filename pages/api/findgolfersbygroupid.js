import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export default async function handler(req, res) {
  if (req.method === "POST") {
    await prisma.golfer
      .findMany({
        where: {
          group_id: parseInt(req.body.data.group_id, 10),
        },
      })
      .then((results) => {
        res.status(200).json(results);
      })
      .finally(async () => {
        await prisma.$disconnect();
      });
  }
}
