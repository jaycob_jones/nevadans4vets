import { PrismaClient } from "@prisma/client";

export default async function handler(req, res) {
  const prisma = new PrismaClient();
  const { data } = req.body;
  if (req.method === "POST") {
    console.log(data);
    delete data.membership_player_1;
    await prisma.orders
      .create({
        data,
      })
      .finally(async () => {
        await prisma.$disconnect();
      });
    res.status(200).json({ name: "John Doe" });
  }
}
