import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

export default async function handler(req, res) {
  if (req.method === "POST") {
    const { data } = req.body;

    await prisma.groups
      .findFirst({
        where: {
          group_id: parseInt(data.group_id, 10),
        },
      })
      .then((results) => {
        res.status(200).json(results);
      })
      .catch((error) => {
        res.status(400);
      })
      .finally(async () => {
        await prisma.$disconnect();
      });
  }
}
